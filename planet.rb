require 'pp'

class Planet
	attr_accessor :id, :owner, :type, :droids, :neighbours

	state_machine :goal_state, :initial => :idle, :namespace => 'goal' do
		
		event :reset do
			transition all - [:idle] => :idle
		end
		
		event :attack do
			transition all - [:attacking] => :attacking
		end
		
		event :expand do
			transition all - [:expanding] => :expanding
		end
		
		event :support do
			transition all - [:supporting] => :supporting
		end
		
		event :grow do
			transition all - [:growing] => :growing
		end
		
		state :idle do
			def goal_actions
				puts "P #{@id} > goal_action > :idling" if $verbose
				
				[]
			end
		end
		
		state :attacking do
			def goal_actions
				if self.my_own?
					puts "P #{@id} > goal_action > :attacking"
					
					return attack_actions
				else
					puts "P #{@id} > goal_action > will not attack, because Planet is not mine (#{@owner})" if $verbose
					reset_goal
					return []
				end
			end
		end
		
		state :expanding do
			def goal_actions
				if self.my_own?
					puts "P #{@id} > goal_action > :expanding"
					
					return expand_actions
				else
					puts "P #{@id} > goal_action > will not expand, because Planet is not mine (#{@owner})"
					reset_goal
					return []
				end
			end
		end
		
		state :supporting do
			def goal_actions
				if self.my_own?
					puts "P #{@id} > goal_action > :supporting"
					
					return support_actions
				else
					puts "P #{@id} > goal_action > will not support, because Planet is not mine (#{@owner})"
					reset_goal
					return []
				end
			end
		end
		
		state :growing do
			def goal_actions
				if self.my_own?
					puts "P #{@id} > goal_action > :grow"
					
					return grow_actions
				else
					puts "P #{@id} > goal_action > will not grow, because Planet is not mine (#{@owner})"
					reset_goal
					return []
				end
			end
		end
		
		
	end
	
	state_machine :strategy_state, :initial => :idle do
		
		event :act_aggressive do
			transition all - [:aggressive] => :aggressive
		end
		
		event :act_balanced do
			transition all - [:balanced] => :balanced
		end
		
		event :act_passive do
			transition all - [:passive] => :passive
		end
		
		state :idle do
			def attack_actions
				puts "P #{@id} > attack_actions > :idling" if $verbose
				[]
			end
			
			def expand_actions
				puts "P #{@id} > expand_actions > :idling" if $verbose
				[]
			end
			
			def support_actions
				puts "P #{@id} > support_actions > :idling" if $verbose
				[]
			end
			
			def grow_actions
				puts "P #{@id} > grow_actions > :idling" if $verbose
				[]
			end
		end
		
		state :aggressive do
			def attack_actions
				aggressive_attack
			end
			
			def expand_actions
				aggressive_expansion
			end
			
			def support_actions
				aggressive_support
			end
			
			def grow_actions
				aggressive_grow
			end
		end
		
		state :balanced do
			def attack_actions
				balanced_attack
			end
			
			def expand_actions
				balanced_expansion
			end
			
			def support_actions
				balanced_support
			end
			
			def grow_actions
				balanced_grow
			end
		end
		
		state :passive do
			def attack_actions
				passive_attack
			end
			
			def expand_actions
				passive_expansion
			end
			
			def support_actions
				passive_support
			end
			
			def grow_actions
				passive_grow
			end
		end
		
	end
	
	# ========= UPDATE =========
	def update
		goal_actions		
	end
	# ========= End of UPDATE =========
	
	def aggressive_attack
		actions = []
		
		hostiles = $w.hostile_neighbours_of(self) + $w.neutral_neighbours_of(self)
		
		puts "\nP #{@id} (o: #{@owner}) No Hostiles Trace:" if hostiles.empty? &&  $verbose
		pp $w.all_neighbours_of(self).select{|n| n.owner} if hostiles.empty?
		
		return [] if hostiles.empty?
		
		troops_available = @droids - population_max * $aggressive_troops_pct
		troops_available = 9 if troops_available < 1
		
		# paths = attack_paths_for []
		
		# path = paths.select{|p| p[-1].droids < self.droids}.max{|p| p[-1].strength}
		# hostile = path.nil? ? hostiles.max{|hst| - hst.droids * 2 + hst.strength * 6} : path[2]
		
		hostile = hostiles.select{|p| p.droids < self.droids}.max{|hst| - hst.droids * 2 + hst.strength * 6}
		
		actions << {"from" => @id, "to" => hostile.id, "unitscount" => troops_available.to_i} unless hostile.nil?
		
		puts "P #{@id} (o: #{@owner}) attacking aggressively!"
		
		actions
	end
	
	def balanced_attack
		actions = []
		
		hostiles = ($w.hostile_neighbours_of self).sort_by!{|hst| - hst.droids * 2 + hst.population_max * 6}
		
		puts "\nP #{@id} (o: #{@owner}) No Hostiles Trace:" if hostiles.empty? &&  $verbose
		pp $w.all_neighbours_of(self).select{|n| n.owner} if hostiles.empty? &&  $verbose
		
		return [] if hostiles.empty?
		
		troops_available = @droids - population_max * $balanced_troops_pct
		troops_available = 8 if troops_available < 1
		
		hostiles.each{|hst|
			troops_available = troops_available * 0.5
			break if troops_available.to_i == 0
			actions << {"from" => @id, "to" => hst.id, "unitscount" => troops_available.to_i}
		}
		
		puts "P #{@id} (o: #{@owner}) attacking balanced"
		
		actions
	end
	
	def passive_attack
		actions = []
		
		hostile = ($w.hostile_neighbours_of self).min{|hst| hst.droids}
		
		puts "\nP #{@id} (o: #{@owner}) No Hostiles Trace:" if hostile.nil? &&  $verbose
		pp $w.all_neighbours_of(self).select{|n| n.owner} if hostile.nil? && $verbose
		
		return [] if hostile.nil?
		
		troops_available = @droids - population_max * $passive_troops_pct
		troops_available = 12 if troops_available < 12
		
		actions << {"from" => @id, "to" => hostile.id, "unitscount" => troops_available.to_i}
		
		puts "P #{@id} (o: #{@owner}) attacking passively"
		
		actions
	end
	
	def aggressive_support
		actions = []
		
		allies = $w.my_own_neighbours_of self
		
		return [] if allies.empty?
		
		troops_available = @droids - population_max * $aggressive_troops_pct
		troops_available = 3 if troops_available < 1
		
		allie = allies.max{|a| a.strength + a.vacancy_level*4}
		
		actions << {"from" => @id, "to" => allie.id, "unitscount" => troops_available.to_i}
		
		puts "P #{@id} (o: #{@owner}) supporting aggressively!"
		
		actions
	end
	
	def balanced_support
		actions = []
		
		allies = ($w.my_own_neighbours_of self).sort_by!{|a| a.droids*2 + a.population_max * 4}
		
		return [] if allies.empty?
		
		troops_available = @droids - population_max * $balanced_troops_pct
		troops_available = 2 if troops_available < 1
		
		allies.each{|allie|
			troops_available = troops_available * 0.5
			break if troops_available.to_i == 0
			actions << {"from" => @id, "to" => allie.id, "unitscount" => troops_available.to_i}
		}
		
		puts "P #{@id} (o: #{@owner}) supporting balanced"
		
		actions
	end
	
	def passive_support
		actions = []
		
		allie = ($w.my_own_neighbours_of self).min{|a| a.droids}
		
		return [] if allie.nil?
		
		troops_available = @droids - population_max * $passive_troops_pct
		troops_available = 1 if troops_available < 1
		
		actions << {"from" => @id, "to" => allie.id, "unitscount" => troops_available.to_i}
		
		puts "P #{@id} (o: #{@owner}) supporting passively"
		
		actions
	end
	
	def aggressive_expansion
		actions = []
		
		neutrals = $w.neutral_neighbours_of self
		return [] if neutrals.empty?
		
		troops_available = @droids - population_max * $aggressive_troops_pct
		troops_available = 11 if troops_available < 11
		
		neutral = neutrals.max{|n| n.population_max}		
		actions << {"from" => @id, "to" => neutral.id, "unitscount" => troops_available.to_i}
		
		puts "P #{@id} (o: #{@owner}) expanding aggressively!"
		
		actions
	end
	
	def balanced_expansion
		actions = []
		
		neutrals = $w.neutral_neighbours_of self
		return [] if neutrals.empty?
				
		neutrals.sort_by!{|neut| - neut.population_max}
		
		troops_available = @droids - population_max * $balanced_troops_pct
		troops_available = 5 if troops_available < 5
		
		neutrals.each{|neut|
			troops_available = troops_available * 0.5
			break if troops_available.to_i == 0
			actions << {"from" => @id, "to" => neut.id, "unitscount" => troops_available.to_i}
		}
		
		puts "P #{@id} (o: #{@owner}) expanding balanced"
		
		actions
	end
	
	def passive_expansion
		actions = []
		
		neutrals = $w.neutral_neighbours_of self
		return [] if neutrals.empty?
		
		neutral = neutrals.max{|neut| neut.strength}
		
		troops_available = @droids - population_max * $passive_troops_pct
		troops_available = 3 if troops_available < 3
		
		actions << {"from" => @id, "to" => neutral.id, "unitscount" => troops_available.to_i}
		
		puts "P #{@id} (o: #{@owner}) expanding passively"
		
		actions
	end
	
	def aggressive_grow
	
		puts "P #{@id} (o: #{@owner}) growing aggressively!"
		
		[]
	end
	
	def balanced_grow
	
		puts "P #{@id} (o: #{@owner}) growing balanced"
		
		[]
	end
	
	def passive_grow
	
		puts "P #{@id} (o: #{@owner}) growing passively"
		
		[]
	end
	
	def attack_profit
		hostiles = $w.hostile_neighbours_of self
		neutrals = $w.neutral_neighbours_of self
		
		puts "P #{@id} (o: #{@owner}) attack_profit 0 due to no hostiles" if hostiles.empty? && $verbose
		return 0 if hostiles.empty?
		
		#return ($atk_profit_factor * 0.1).to_i
		
		# sum = 0.0
		# hostiles.each{|h|
			# sum = sum + h.vacancy_level
		# }
		sum = 2.0
		
		sum = sum + 6 if hostiles.length == 1 && hostiles[0].droids < self.droids
		sum = sum + 1 if hostiles.length == 2
		sum = sum - 1 if hostiles.length == 3
		sum = sum + 3 if hostiles.max{|h| h.strength}.strength == 4 && self.strength < 4
		
		profit = ($atk_profit_factor * sum).to_i
		
		puts "attack_profit: #{profit}" if $verbose
		
		balanced_attack if vacancy_level < 0.3 && profit < 10
		
		puts "\nDONE UNPLANNED BALANCED ATTACK\n\n" if vacancy_level < 0.3
		
		profit
	end
	
	def expand_profit
		neutrals = $w.neutral_neighbours_of self
		hostiles = $w.hostile_neighbours_of self
		
		puts "P #{@id} (o: #{@owner}) expand_profit 0 due to no neutrals", @neighbours if neutrals.empty? && $verbose
		return 0 if neutrals.empty?
		
		# sum = 0.0
		# neutrals.each{|n|
			# sum = sum + n.strength / 4.0
		# }
		sum = 1.0
		
		sum = sum + 2.0 if neutrals.length == 1 && neutrals[0].population_max > population_max
		sum = sum + 2.0 unless hostiles.empty? || neutrals.max{|n| n.population_max}.population_max < hostiles.max{|h| h.population_max}.population_max
		sum = sum + 1.25 if neutrals.length == 3
		
		profit = ($expand_profit_factor * sum).to_i
		
		puts "expand_profit: #{profit}" if $verbose
		
		profit
	end
	
	def support_profit
		allies = $w.my_own_neighbours_of self
		
		puts "P #{@id} (o: #{@owner}) support_profit 0 due to no allies", @neighbours if allies.empty? && $verbose
		return 0 if allies.empty?
		
		sum = 0
		allies.each{|a| sum = sum + a.vacancy_level }
		
		sum = sum + 2 if $w.hostile_neighbours_of(self).empty?
		sum = sum + 2 if $w.hostile_neighbours_of(self).length < 2
		
		profit = ($support_profit_factor * sum).to_i
		
		puts "support_profit: #{profit}" if $verbose
		
		balanced_support if vacancy_level < 0.3 && profit < 10
		
		puts "\nDONE UNPLANNED BALANCED SUPPORT\n\n" if vacancy_level < 0.3
		
		profit
	end
	
	def grow_profit
		profit = ($grow_profit_factor * vacancy_level).to_i
		
		puts "grow_profit: #{profit}" if $verbose
		
		profit
	end
	
	def switch_goal

		return unless my_own?
	
		goals = []
		attack_profit.times{|i| goals << :attack_goal}
		expand_profit.times{|i| goals << :expand_goal}
		support_profit.times{|i| goals << :support_goal}
		grow_profit.times{|i| goals << :grow_goal}
		
		goal_event = goals.shuffle.sample
		
		puts "Planet #{@id} (o: #{@owner}) > switch_goal => #{goal_event}"
			
		send(goal_event) unless goals.empty?
	end
	
	def aggressive_profit
		return 4 if @droids / population_max < ($aggressive_troops_pct)
	
		profit = ($aggressive_profit_factor * (1 - (population_max - [@droids, population_max].min) / population_max) / $aggressive_troops_pct).to_i
		
		puts "P #{@id} (o: #{@owner}): aggressive_profit: #{profit}" #if $verbose
		
		profit
	end
	
	def balanced_profit
		return 2 if @droids / population_max < ($balanced_troops_pct)
		
		profit = ($balanced_profit_factor * (1 - (population_max - [@droids, population_max].min) / population_max) / $balanced_troops_pct ).to_i
		
		puts "P #{@id} (o: #{@owner}): balanced_profit: #{profit}" #if $verbose
		
		profit
	end
	
	def passive_profit
		return 1 if @droids / population_max < ($passive_troops_pct)
		
		profit = ($passive_profit_factor * (1 - (population_max - [@droids, population_max].min) / population_max) / $passive_troops_pct).to_i
		
		puts "P #{@id} (o: #{@owner}): passive_profit: #{profit}" #if $verbose
		
		profit
	end
	
	def switch_strategy

		return unless my_own?
	
		strategies = []
		aggressive_profit.times{|i| strategies << :act_aggressive}
		balanced_profit.times{|i| strategies << :act_balanced}
		passive_profit.times{|i| strategies << :act_passive}
		
		strategy_event = strategies.shuffle.sample
		
		puts "P #{@id} (o: #{@owner}) > switch_strategy => #{strategy_event}"
		
		send(strategy_event) unless strategies.empty?
	end
	
	def hostile?
		@is_hostile
	end
	
	def neutral?
		@is_neutral
	end
	
	def my_own?
		@is_my_own
	end
	
	def strength
		case @type
		when "TYPE_A"
			1
		when "TYPE_B"
			2
		when "TYPE_C"
			3
		when "TYPE_D"
			4
		else
			0
		end
	end
	
	def grow_rate
		case @type
		when "TYPE_A"
			0.1
		when "TYPE_B"
			0.15
		when "TYPE_C"
			0.2
		when "TYPE_D"
			0.3
		else
			0
		end
	end
	
	def population_max
		case @type
		when "TYPE_A"
			100.0
		when "TYPE_B"
			200.0
		when "TYPE_C"
			500.0
		when "TYPE_D"
			1000.0
		else
			0.0
		end
	end
	
	def vacancy_level
		v_level = 1.01 - ([@droids, population_max].min / population_max.to_f)
		
		puts "P #{@id} (o: #{@owner}) vacancy_level: #{v_level}" if v_level > 1.01 || v_level < 0.01
		puts "P Type: #{@type} - Droids: #{@droids}" if v_level > 1.01 || v_level < 0.01
		
		v_level
	end
		
	def attack_paths_for pre_path
		pre_path = pre_path + [self] if pre_path.empty?
	
		return [] if pre_path.last.strength > self.strength || pre_path.length > 10
		
		paths = []
		
		neighbours = $w.all_neighbours_of(self).select{|p| p.strength > self.strength}
		neighbours.each{|n|
			paths << pre_path + [self, n]
			paths = paths + n.attack_paths_for(pre_path.include?(self) ? pre_path : pre_path + [self]) unless pre_path.include?(n)
		}
		
		paths
	end
		
	def parse_hash hash
		
		pp "Planet.parse_hash > hash" if $verbose
		pp hash                       if $verbose
	
		@id = hash["id"].to_i
		
		@owner = hash["owner"][0]
		@is_hostile = !@owner.nil? && !@owner.empty? && @owner != $my_name
		@is_my_own = @owner == $my_name
		@is_neutral = @owner.nil? || @owner.empty?
		
		@type = hash["type"][0]
		@droids = hash["droids"][0].to_i
		@neighbours = hash["neighbours"][0]["neighbour"].map{|s| s.to_i}
	end
	
	def to_s
		"P #{@id} (o: #{@owner})"
	end
	
	def initialize
		super() # NOTE: This *must* be called, otherwise states won't get initialized
	end
end