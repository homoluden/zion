require 'ruby-prof'
require 'pp'
require 'socket'
require 'xmlsimple'

require './automata.rb'

$verbose = false


RubyProf.start

bot = Automata.new
bot.init
bot.run

result = RubyProf.stop

# Print a flat profile to text
printer = RubyProf::FlatPrinter.new(result)
printer.print(File.open("profile.log", 'w'))


