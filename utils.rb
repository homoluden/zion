require 'socket'
require 'pp'


$log = []

$error_msg = {
	:not_joined => "User has not joined any game.",
	:content_not_allowed => "Content is not allowed in prolog."
}

$my_name = "Automata Ludens"
$token = 'ekujdtyxik3z61qii6nqq4rbxkr2ww5z'
#$token = 'xlfezsvbmklghswnccllbe4jqj3m66dc'


$initial_request = %Q^<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<request>
	<token>#{$token}</token>
	<actions>
	</actions>
</request>^


class Utils

	def self.compose_actions_xml actions
		h = XmlSimple.xml_in $initial_request
		
		h["actions"][0]["action"] = actions
		
		XmlSimple.xml_out h, {"AttrPrefix" => true, "RootName" => "request"}
	end

	def self.get_errors hash
		
		return [:hash_empty] if hash.empty?
		
		errors = []	
		errors_hash = hash["errors"][0]["error"]
		
		if errors_hash
			errors_hash.each{|e|
				errors << e
			}
		end
		
		errors
	end
	
	
	def self.post_back request
		pp "Automata > post_back"     if $verbose
		
		s = TCPSocket.new '176.192.95.4', 10040
		
		pp request                            if $verbose
		
		$log << request
		
		s.puts request
		
		pp "Automata > post_back > Sent request"     if $verbose
		
		response = []
		
		begin  
			while line = s.gets
			puts line if $verbose
			response << line
				
			resp_string = response * "\n"
			$log << resp_string
			
			s.close
			
			pp response 						   if $verbose
			pp "==============\n"   if $verbose
			
			
			
			hash = XmlSimple.xml_in resp_string
			
			return hash
		end
		rescue Exception => e  
			puts "Error reading response from server:", e.message, e.backtrace
		end  
		
		{}
	end
	
	def self.game_over? errors
		gameovers = errors.select{|h| (h == $error_msg[:not_joined]) || (h.class == Hash && h["type"] == "gameover")}
		
		gameovers.length > 0
	end
end