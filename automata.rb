require 'pp'
require 'socket'
require 'xmlsimple'

require './utils.rb'
require './world.rb'

$step = 1

class Automata
	@initialized = false
	

	def run
	
		begin
		
			actions = []
			
			actions = actions + $w.update if $step > 3
			
			if $mode == :big_center && $step > 0
				atk_paths = $hp.attack_paths_for []
				
				best_path = atk_paths.max{|p| p.last.strength}
				
				puts "Best Path:", best_path.select{|p| p.id}
				
				best_path.each_index{|i|
					next if i<1
									
					from = best_path[i-1].id
					to = best_path[i].id
					
					9.times{|j|
						$predef_actions << {"from" => from, "to" => to, "unitscount" => i == best_path.length - 1 ? 5 : 10}
						$predef_actions << {"from" => from, "to" => to, "unitscount" => i == best_path.length - 1 ? 0 : 1}
					}
					
				}
			end
			
			if $step > $early_stage || ($step < $early_stage && $w.planets.select{|h| h.hostile? && h.strength == 4}.empty?)
				20.times{|i| 
					action = $predef_actions.shift
					break if action.nil?
					actions << action
				} unless $predef_actions.empty? && $step == 1
			else
				my = $w.planets.select{|p| p.strength == 3 && p.my_own?}
				neigh = $w.neutral_neighbours_of my[0] unless my.empty?
				actions << {"from" => my[0].id, "to" => neigh[0].id, "unitscount" => (my[0].droids*0.5).to_i} unless my.empty? || neigh.nil? || neigh.empty?
			end
			
			puts "World updated (STEP #{$step})", "New Actions:"
			pp actions
			
			request = Utils.compose_actions_xml actions
			
			puts "New Request:" if $verbose
			pp request               if $verbose
			
			errors = []
			
			begin  
				response = Utils.post_back request.to_s
				
				next if response.empty?
				
				$w.parse_hash response, true
				
				update_goal_strategy_modifiers
				supper_actions
				
				$w.update_goals_strategies
				
				errors = Utils.get_errors response
			
			rescue Exception => e  
				puts "Error while updating World:", e.message, e.backtrace
			end  
			
			puts "Errors:" unless errors.empty?
			pp errors       unless errors.empty?
			
			#$step = $step+1
			
		end until Utils.game_over?(errors)
		
		File.write('game.log', $log * "\n")
		
	end

	
	
	def init
	
		game_started = false
		begin
			response = Utils.post_back $initial_request
			
			errors = Utils.get_errors response
			
			pp "Automata > init > errors:" if $verbose
			pp errors                               if $verbose
			puts '...'                                  unless $verbose
			
			next if errors.include? :hash_empty
			
			game_started = !(errors.include? $error_msg[:not_joined])
			
		end until game_started
		
		puts "Joined the game at #{Time.now.strftime("%d/%m/%Y %H:%M")}"
		
	
		$w = World.new
		$w.parse_hash response, false
		
		my_planets = $w.planets.select{|p| p.my_own?}
			
		puts "Got more than one owned planet at first step" if my_planets.length > 1
		
		$hp = my_planets[0]
		
		$mode = $hp.strength < 4 ? :big_center : :big_bases
	
		$predef_actions = []
		
		# if $mode == :big_center
			# atk_paths = $hp.attack_paths_for []
			
			# best_path = atk_paths.max{|p| p.last.strength}
			
			# puts "Best Path:", best_path.select{|p| p.id}
			
			# best_path.each_index{|i|
				# next if i<1
								
				# from = best_path[i-1].id
				# to = best_path[i].id
				
				# 10.times{|j|
					# $predef_actions << {"from" => from, "to" => to, "unitscount" => i == best_path.length - 1 ? 5 : 10}
					# $predef_actions << {"from" => from, "to" => to, "unitscount" => i == best_path.length - 1 ? 0 : 1}
				# }
				
			# }
		# end
		
	end

	$early_stage, $mid_stage = 25, 50
	
	def supper_actions
		hostiles = $w.planets.select{|p| p.hostile?}
		
		if hostiles.length < 4
			hostiles.each{|h|
				my_planets = $w.my_own_neighbours_of(h)
				
				my_planets.each{|mp|
					3.times{|j|
						$predef_actions << {"from" => mp.id, "to" => h.id, "unitscount" => (h.droids*0.4).to_i+1}
					}
				}
			}
		end
		
		allies = $w.planets.select{|p| p.my_own? && p.strength == 4}
		allies.each{|a|
			a.aggressive_attack if a.vacancy_level > 0.8
		}
	end
	
	def update_goal_strategy_modifiers
		$step = 1 if $step.nil?
		$step = $step + 1
		
		if $mode == :big_center
			$atk_profit_factor = $step < $early_stage ? 10 : ($step < $mid_stage ? 25 : 70)
			$expand_profit_factor = $step < $early_stage ? 10 : ($step < $mid_stage ? 20 : 1)
			$support_profit_factor = $step < $early_stage ? 25 : ($step < $mid_stage ? 35 : 40)
			$grow_profit_factor = $step < $early_stage ? 90 : ($step < $mid_stage ? 40 : 25)
			
			$aggressive_profit_factor = $step < $early_stage ? 10 : ($step < $mid_stage ? 25 : 70)
			$balanced_profit_factor = $step < $early_stage ? 50 : ($step < $mid_stage ? 45 : 20)
			$passive_profit_factor = $step < $early_stage ? 40 : ($step < $mid_stage ? 15 : 5)
			
			$aggressive_troops_pct = $step < $early_stage ? 0.4 : ($step < $mid_stage ? 0.35 : 0.3)
			$balanced_troops_pct = $step < $early_stage ? 0.65 : ($step < $mid_stage ? 0.5 : 0.4)
			$passive_troops_pct = 0.85
			
		else
			
			$atk_profit_factor = $step < $early_stage ? 20 : ($step < $mid_stage ? 30 : 75)
			$expand_profit_factor = $step < $early_stage ? 5 : ($step < $mid_stage ? 20 : 5)
			$support_profit_factor = $step < $early_stage ? 35 : ($step < $mid_stage ? 35 : 45)
			$grow_profit_factor = $step < $early_stage ? 65 : ($step < $mid_stage ? 50 : 25)
			
			$aggressive_profit_factor = $step < $early_stage ? 10 : ($step < $mid_stage ? 30 : 90)
			$balanced_profit_factor = $step < $early_stage ? 50 : ($step < $mid_stage ? 30 : 20)
			$passive_profit_factor = $step < $early_stage ? 30 : ($step < $mid_stage ? 20 : 10)
			
			$aggressive_troops_pct = $step < $early_stage ? 0.5 : ($step < $mid_stage ? 0.4 : 0.3)
			$balanced_troops_pct = $step < $early_stage ? 0.75 : ($step < $mid_stage ? 0.6 : 0.4)
			$passive_troops_pct = 0.85
		end
	end
	
end