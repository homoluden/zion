require 'pp'
require 'state_machine'


require "./planet.rb"
require "./automata.rb"

class World
	attr_accessor :planets
	
	@updates_before_goal_reset = $max_updates_before_goal_reset
	@planets = []
	
	def update
	
		actions = []
		
		@planets.each{|p| actions = actions + p.update}
		
		actions
	end
	
	def all_neighbours_of planet
		neigh_ids = planet.neighbours
		
		@planets.select{|p| neigh_ids.include?(p.id)}
	end
	
	def hostile_neighbours_of planet
		neigh_ids = planet.neighbours
		
		@planets.select{|p| neigh_ids.include?(p.id) && p.hostile? }
	end
	
	def neutral_neighbours_of planet
		neigh_ids = planet.neighbours
		
		@planets.select{|p| neigh_ids.include?(p.id) && p.neutral? }
	end
	
	def my_own_neighbours_of planet
		neigh = planet.neighbours
		
		@planets.select{|p| neigh.include?(p.id) && p.my_own? }
	end
	
	def my_own_planets
		@planets.select{|p| p.my_own? }
	end
	
	def update_goals_strategies
		@planets.each{|p| p.switch_goal; p.switch_strategy; }
	end
	
	def parse_hash(hash, update)
		
		errors = []		
		errors_hash = hash["errors"][0]["error"]
		
		if errors_hash
			errors_hash.each{|e|
				errors << e
			}
		end
		
		@planets = [] unless update
		planets_hash = hash["planets"][0]["planet"]
		
		if planets_hash
			planets_hash.each{|h|
			
				puts "World > parse_hash > each > h" if $verbose
				pp h                                                 if $verbose
				
				planet = nil
				unless update
					planet = Planet.new
					@planets << planet
				else
					planet = @planets.select{|p| p.id == h["id"].to_i}[0]
				end
				
				planet.parse_hash h
				
			}
		end
	end
	
	def parse_xml(xml_str, update)
		hash = XmlSimple.xml_in xml_str

		pp "parse_xml > hash" if $verbose
		pp hash                       if $verbose
		
		parse_hash hash
	end
	
end